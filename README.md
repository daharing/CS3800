# CS3800 Repository

This repository contains all my code (homeworks + project) for the Missouri S&T Operating Systems class (CS3800).

Due to the size and scope of the homeworks and project as well as for organizational purposes, each is contained on a separate branch. As a result, the master branch does not contain anything and should not be used.

The setup of this repo is tailored for Linux (I specifically used Ubuntu) and as such any makefiles you run probably won't run on Windows. The project specifically needs to run on Linux. Should you attempt to use Windows I would suggest using the CygWin terminal.

## Branches
| Branch | Description |
| ------ | -----------|
| master                      | Contains README |
| final              | First CS3800 programming assignment. Create C++ program to imitate Linux filesystem. Supported commands are cd, chmod, ls, mkdir, pwd, rm, rmdir, and touch. |
| hw2_f3                      | Second CS3800 programming assignment. Implement file permissions, users, groups. Program built on top of the first assignment. |
| hw2_f3_final                | Second assignment submission branch. |
| hw3_writingPhils            | Second CS3800 programming assignment. Implement a  |
| project_bootloader_dev      | Contains my bootloader + simple kernel code that runs on an x86 system for my CS3800 project. This is a fairly big branch and will be moved to its own repo. |
| project_multiboot_bootloader| Contains a simple kernel that runs on x86 and uses a multiboot bootloader for the CS3800 project. This will be moved to its own repo. |

## Contributing
Please contact me.

## License
[MIT](https://choosealicense.com/licenses/mit/)